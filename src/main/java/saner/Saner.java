package saner;

import au.com.southsky.jfreesane.*;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

public class Saner {
    public static void main(String[] args) throws IOException, SaneException {
        System.out.println("Testing SANE daemon");
        SaneSession session = SaneSession.withRemoteSane(
                InetAddress.getByName("127.0.0.1")
        );
        List<SaneDevice> devices = session.listDevices();
        System.out.println(devices.size());
        devices.stream().forEach(v-> System.out.println(v.toString()));
        SaneDevice device = devices.get(0);
        device.open();
        System.out.println("Options for "+device.toString());
        List<SaneOption> options = device.listOptions();
        options.stream().forEach(c-> {
            System.out.println(c);
            SaneOption option = null;
            try {
                option = device.getOption(c.getName());
                List<String> validValues = option.getStringConstraints();
                if(validValues != null) {
                    System.out.println("\t"+validValues);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        device.getOption("source").setStringValue("ADF Back");
        System.out.println("Starting ADF scan");
        while (true) {
            try {
                BufferedImage image = device.acquireImage();
                System.out.println(image);
            } catch (SaneException e) {
                if (e.getStatus() == SaneStatus.STATUS_NO_DOCS) {
                    // this is the out of paper condition that we expect
                    System.out.println("NO DOCS reached");
                    break;
                } else {
                    // some other exception that was not expected
                    throw e;
                }
            }
        }

    }
}
